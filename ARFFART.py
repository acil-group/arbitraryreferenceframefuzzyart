import numpy as np
from sklearn.decomposition import IncrementalPCA
import itertools


def norm(x):
    return np.linalg.norm(x,1)

def CC(x):
    return np.hstack((x,1-x))


class ARFFART_cluster:
    def __init__(self, x=None):
        self.dim = None
        self.center = None
        self.bounding_box = None
        self.n = 0
        self.pca = None
        self.batch = []
        self.first_batch = True
        if x is not None:
            self.add_sample(x)

    def get_corners_from_bounding_box(self):
        min_x = self.bounding_box[:self.dim]
        max_x = 1 - self.bounding_box[:self.dim]
        minmax_x = np.vstack([min_x,max_x])
        bvals = itertools.product([0,1],repeat=self.dim)
        corners = [[minmax_x[j,i] for i,j in enumerate(bval)] for bval in bvals]
        return np.vstack(corners)

    def inverse_transform_bounding_box(self):
        corners = self.get_corners_from_bounding_box()
        return self.pca.inverse_ransform(corners)

    def transform_bounding_box(self, inverse_transformed_corners):
        transformed_corners = self.pca.transform(inverse_transformed_corners)
        self.bounding_box = np.min(CC(transformed_corners))

    def expand_bounding_box(self, ccx, beta):
        new_bounding_box = np.minimum(self.bounding_box,ccx)
        self.bounding_box = beta*new_bounding_box + (1.0-beta)*self.bounding_box

    def add_sample(self,x,beta):
        if self.dim is None:
            self.dim = x.shape[1]
            self.center = x
            self.bounding_box = CC(x-self.center)
            self.pca = IncrementalPCA(n_components=self.dim)
            self.batch = [None]*self.dim
        assert self.dim == x.shape[1]
        self.batch[self.n%self.dim] = x
        self.center = (self.center*self.n + x) / (self.n+1)
        self.n += 1
        if not self.n%self.dim:
            if not self.first_batch:
                inverse_transformed_corners = self.inverse_transform_bounding_box()
            else:
                inverse_transformed_corners = self.get_corners_from_bounding_box()
            self.pca.partial_fit(np.vstack(self.batch))
            self.transform_bounding_box(inverse_transformed_corners)
            self.first_batch = False
        self.expand_bounding_box(CC(self.pca.transform(x)), beta)

    def activation(self,x,alpha):
        if self.first_batch:
            pcax = x-self.center
        else:
            pcax = self.pca.transform(x)
        return norm(np.minimum(pcax,self.bounding_box))/(alpha+norm(self.bounding_box))


    def resonance(self,x,rho):
        if self.first_batch:
            pcax = x-self.center
        else:
            pcax = self.pca.transform(x)
        return norm(np.minimum(pcax,self.bounding_box))/(norm(pcax)) >= rho



class ARFFART:
    def __init__(self, rho, alpha, beta):
        self.rho = rho
        self.alpha = alpha
        self.beta = beta

        self.clusters = []

    def fit_single(self,x):
        i = 0
        if len(self.clusters) > 0:
            activation = [cluster.activation(x,self.alpha) for cluster in self.clusters]
            sidx = np.argsort(activation)[::-1]
            while i < len(sidx) and not self.clusters[sidx[i]].resonance(x,self.rho):
                    i += 1
            if i >= len(sidx):
                self.clusters.append(ARFFART_cluster(x))
            else:
                self.clusters[sidx[i]].add_sample(x,self.beta)
        else:
            self.clusters.append(ARFFART_cluster(x))

        return i

    def fit(self, X, max_epochs):
        labels = [-1]*X.shape[0]
        for e in range(max_epochs):
            old_labels = list(labels)
            for i,x in enumerate(X[:,None]):
                labels[i] = self.fit_single(x)
            if all(old_labels==labels):
                break
        return labels

    def predict_single(self,x):
        i = 0
        if len(self.clusters) > 0:
            activation = [cluster.activation(x, self.alpha) for cluster in self.clusters]
            sidx = np.argsort(activation)[::-1]
            while i < len(sidx) and not self.clusters[sidx[i]].resonance(x, self.rho):
                i += 1
            if i >= len(sidx):
                ri = -1
        else:
            i = -1
        return i

    def predict(self,X):
        labels = [-1]*X.shape[0]
        for i,x in enumerate(X[:,None]):
            labels[i] = self.predict_single(x)
        return labels
