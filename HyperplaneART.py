import numpy as np



class Dense:
    def __init__(self, input_units, output_units, learning_rate=10e-4):
        self.learning_rate = learning_rate
        self.weights = np.random.normal(loc=0.0, scale=np.sqrt(2/(input_units+output_units)), size= (input_units, output_units))
        self.biases = np.zeros(output_units)

    def forward(self, input):
        return np.dot(input, self.weights) + self.biases

    def backward(self, input, grad_output):
        grad_input = np.dot(grad_output, self.weights.T)
        grad_weights = np.dot(input.T, grad_output)
        grad_biases = grad_output.mean(axis=0)*input.shape[0]
        assert grad_weights.shape == self.weights.shape
        assert  grad_biases.shape == self.biases.shape
        self.weights -= self.learning_rate*grad_weights
        self.biases -= self.learning_rate*grad_biases
        return grad_input

class iterative_linear_approximator:
    def __init__(self, input_units, output_units, learning_rate=10e-4):
        self.layer = Dense(input_units, output_units, learning_rate)

    def predict(self, x):
        return self.layer.forward(x)

    def update(self, x, y):
        y_hat = self.predict(x)
        err = y_hat-y
        self.layer.backward(x,err)


def calc_rotation_and_translation(W, b, W_old, b_old):
    translation = b-b_old
    rotation = np.arctan(W) - np.arctan(W_old)
    return rotation, translation

def create_n_dim_rotation_matrix(n_dim, angles):
    pass


def rotate_and_translate_pont(point, thetas, dy):
    n_dim = len(point)
    new_point = point + dy
    R = create_n_dim_rotation_matrix(n_dim, thetas)
    new_point = R*point
    return new_point