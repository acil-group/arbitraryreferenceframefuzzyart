from sklearn.decomposition import IncrementalPCA
from sklearn.datasets import make_blobs
import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial.transform import Rotation

def gen_batch(X,batch_size=1):
    batch = [None]*batch_size
    n = 0
    for x in X:
        batch[n] = x
        n = (n+1) % batch_size
        if not n:
            yield np.vstack(batch)


if __name__ == '__main__':
    X, y = make_blobs(n_samples=300, centers=1, n_features=3, random_state = 0)
    X[:,1] /= 4
    X[:,2] /= 5
    X += np.array([1,3,2])
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    ax.set_xlim([-6,6])
    ax.set_ylim([-6,6])
    ax.set_zlim([-6,6])


    R = Rotation.from_rotvec(np.pi/3 * np.array([0,1,0.5])).as_matrix()
    print(R)

    X = np.matmul(X,R)


    my_pca = IncrementalPCA(n_components=3)
    my_pca.fit(X[:3,:])
    center = np.mean(X[:3,:])
    n = 3
    for x in gen_batch(X[3:,None],3):
        ax.clear()
        ax.scatter(X[:, 0], X[:, 1], X[:, 2])
        my_pca.partial_fit(x)
        # print(my_pca.components_)
        center = (center*n + x)/(n+1)
        center = np.mean(X,axis=0)
        print(my_pca.components_)
        for axis in my_pca.components_:
            axis *= 5
            plt.plot([center[0],axis[0]+center[0]],[center[1],axis[1]+center[1]],[center[2],axis[2]+center[2]],'r-')
        n += 3
        print(n)
        plt.pause(0.5)
    plt.show()